import React, { useEffect } from "react";
import Hotkeys from "react-hot-keys";
import prettier from "prettier/standalone";
import babylon from "prettier/parser-babel";
import htmlylon from "prettier/parser-html";
import Editor from "react-simple-code-editor";
import { highlight, languages } from "prismjs";
import "prismjs/components/prism-markup";
import "prismjs/components/prism-css";
import "prismjs/components/prism-javascript";
// import "prismjs/themes/prism-tomorrow.css";
import "prismjs/themes/prism-okaidia.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.scss";

function App() {
  const [code, setCode] = React.useState(
    localStorage.getItem("code") ? localStorage.getItem("code") : ""
  );
  const [toggle, setToggle] = React.useState(false);

  const formatCode = () => {
    const formattedCode = prettier.format(code.toString(), {
      parser: "html",
      plugins: [babylon, htmlylon],
    });

    setCode(formattedCode);
  };

  useEffect(() => {
    const updateLocalStorage = () => {
      localStorage.setItem("code", code);
    };

    updateLocalStorage();
  }, [code]);

  const toggleFrame = () => {
    setToggle(!toggle);
  };

  return (
    <div className="app">
      <header className="app-header">
        <p>
          <code>HTML/CSS/JS</code> sandbox
        </p>
        <button className="btn btn-success" onClick={formatCode}>
          Format Code
        </button>
      </header>
      <div className="row no-gutters app-wrapper">
        <div className={toggle ? "col-1" : "col-4"}>
          <div className="code-editor">
            {code.length < 1 ? (
              <p className="code-editor-placeholder">Paste your code here...</p>
            ) : (
              <></>
            )}
            <Hotkeys
              keyName="command+s,ctrl+s,option+s"
              onKeyDown={formatCode}
              onKeyUp={formatCode}
            >
              <Editor
                value={code}
                onValueChange={(code) => setCode(code)}
                highlight={(code) => highlight(code, languages.markup)}
                padding={10}
                style={{
                  fontFamily: '"Fira code", "Fira Mono", monospace',
                  fontSize: 14,
                }}
                textareaId="CodeEditor"
                textareaClassName="code-editor-textarea"
                preClassName="code-editor-pre"
              />
            </Hotkeys>
          </div>
          <button className="toggle-editor" onClick={toggleFrame}>
            {toggle ? (
              <svg viewBox="0 0 32 32" aria-hidden="true">
                <path d="M18.629 15.997l-7.083-7.081L13.462 7l8.997 8.997L13.457 25l-1.916-1.916z" />
              </svg>
            ) : (
              <svg viewBox="0 0 32 32" aria-hidden="true">
                <path d="M14.19 16.005l7.869 7.868-2.129 2.129-9.996-9.997L19.937 6.002l2.127 2.129z" />
              </svg>
            )}
          </button>
        </div>
        <div className={toggle ? "col-11" : "col-8"}>
          {code.length < 1 ? (
            <p className="code-render-placeholder">
              Anxious robots waiting for your code
              <span role="img" aria-label="emojis">
                🤖 🔌
              </span>
            </p>
          ) : (
            <iframe
              srcDoc={code}
              title="Render Code"
              className="code-render"
            ></iframe>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
